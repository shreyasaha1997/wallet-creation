from django.conf.urls import url
from . import views
app_name = 'keys'
urlpatterns = [
    url(r'^$', views.input, name= 'input'),
    url(r'^account_created$',views.account_created, name='account_created'),
    url(r'^login$',views.login, name='login'),
    ]