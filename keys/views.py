import os
from django.shortcuts import render
from .models import account_details
from hexbytes import HexBytes
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt
from web3.auto import w3
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP
import requests
import base64
from Crypto.PublicKey import RSA

def input(request):
    return render(request, 'keys/input.html')
    

@csrf_exempt
def account_created(request):
    if request.method == 'POST':
        user_id = request.POST.get('User_Id', None)
        password = request.POST.get('password',None)
        acc = account_details.objects.all()
        for a in acc:
           if a.username == user_id:
               return HttpResponse("account is already there")
        acct = w3.eth.account.create(user_id)
        secret_key = password
        cipher = AES.new(secret_key.encode("utf8"),AES.MODE_ECB)
        public_key = acct.address
        private_key = acct.privateKey.hex()
        private_key = private_key.rjust(32) + '00000000000000'
        print(private_key)
        private_key = private_key.encode("utf8")
        print(private_key)
        encoded_private_key = base64.b64encode(cipher.encrypt(private_key))
        account = account_details()
        account.username = user_id
        account.private_key = encoded_private_key
        account.save()       
        return HttpResponse("Account created with public key " + public_key)
    return HttpResponse("Not a post request")

    









